/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author _Enrique_
 */
public class Anfitrion {
    
    private double id;
    private int numAf;
    private String nombre;
    private int evento;
    private int dia;
    private int mes;
    private int año;
    private String Fecha;
    private String email;
    private int status;
    
    //constructores
    public Anfitrion(double id, int numAf, String nombre, int evento, String fecha, String email, int status){
        
        this.id=id;
        this.numAf=numAf;
        this.nombre=nombre;
        this.evento=evento;
        this.dia=dia;
        this.mes=mes;
        this.año=año;
        this.email=email;
        this.status=status;
    }
    
    public Anfitrion(){
        
    }

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public int getNumAf() {
        return numAf;
    }

    public void setNumAf(int numAf) {
        this.numAf = numAf;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEvento() {
        return evento;
    }

    public void setEvento(int evento) {
        this.evento = evento;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String Fecha) {
        this.Fecha = Fecha;
    }

   
    
    
}
