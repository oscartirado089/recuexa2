/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author _Enrique_
 */
public class dbAnfitrion extends dbManejador implements dbPersistencia{
    
    @Override
    public void insertar(Object objeto) throws Exception {
        Anfitrion user = new Anfitrion();
        user = (Anfitrion) objeto;
      
        String consulta = "insert into " + "anfitrion02(numAf,nombre, evento, fecha, email, status) values (?, ?, ?, ?, ?, ?)";
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);
                //Asignar valores a la consulta
                this.sqlConsulta.setInt(1, user.getNumAf());
                this.sqlConsulta.setString(2, user.getNombre());
                this.sqlConsulta.setInt(3, user.getEvento());
   
                this.sqlConsulta.setString(4,user.getFecha());
                this.sqlConsulta.setString(5,user.getEmail());
                this.sqlConsulta.setInt(6,user.getStatus());
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch (SQLException e){
                System.err.println("Surgió un error al insertar" + e.getMessage());
            }
        }
        else{
            System.out.println("No fue posible conectarse ");
        }
    }

    @Override
    public void actualizar(Object objeto) throws Exception {
        Anfitrion user = new Anfitrion();
        user = (Anfitrion) objeto;
     
        
        
        String consulta = "update Anfitrion02 set numAf=?, nombre = ?, evento=?,fecha=?, email=?, status=? where numAf = ?";
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);
                //Asignar valores a la consulta

                this.sqlConsulta.setInt(1, user.getNumAf());
                this.sqlConsulta.setString(2, user.getNombre());
                this.sqlConsulta.setInt(3, user.getEvento());   
                this.sqlConsulta.setString(4,user.getFecha());
                this.sqlConsulta.setString(5,user.getEmail());
                this.sqlConsulta.setInt(6,user.getStatus());
                this.sqlConsulta.executeUpdate();
                this.desconectar();
                
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch (SQLException e){
                System.err.println("Surgió un error al insertar" + e.getMessage());
            }
        }
        else{
            System.out.println("No fue posible conectarse ");
        }
    }



    @Override
    public boolean isExiste(String codigo) throws Exception {
        boolean confirm = false;
        if(this.conectar()){
            String consulta = "Select * from Anfitrion02 where numAf = ?";
            this.sqlConsulta = conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            this.registros = this.sqlConsulta.executeQuery();
            if(this.registros.next()){
                confirm = true;
            }
        }
        this.desconectar();
        return confirm;
    }



    @Override
    public Object buscar(String codigo) throws Exception {
        Anfitrion user = new Anfitrion();
        if(conectar()){
            String consulta = "SELECT * from Anfitrion02 WHERE numAf = ?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);

            this.registros = this.sqlConsulta.executeQuery();
            if(this.registros.next()){
                
                user.setNumAf(this.registros.getInt("numAf"));
                user.setNombre(this.registros.getString("nombre"));
                user.setEvento(this.registros.getInt("evento"));
                user.setFecha(this.registros.getString("fecha"));
                user.setEmail(this.registros.getString("email"));
                user.setStatus(this.registros.getInt("status"));
                
                
            }
        }
        this.desconectar();
        return user;
    }

    @Override
    public void borrar(Object objeto, String codigo) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ArrayList listar() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int deshabilitar(Anfitrion R, String codigo) throws Exception {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        
         Anfitrion user = new Anfitrion();
        if(conectar()){
            String consulta = "Update Anfitrion02 set status = 1 WHERE numAf = ?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);

            this.registros = this.sqlConsulta.executeQuery();
            this.desconectar();
            
  
        }
        return 0;
    }
    
}
